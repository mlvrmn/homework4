﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Text.RegularExpressions;

namespace HomeWork4
{
    public class Parser
    {
        public String GetHtml(String site)
        {
            String html = String.Empty;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(site);

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();

            using (StreamReader reader = new StreamReader(response.GetResponseStream(), Encoding.UTF8))
            {
                 html = reader.ReadToEnd();
            }

            return html;
        }

        public List<String> ParseUrl(String html)
        {
            Regex reg = new Regex(@"(https ?:\/\/[\w\-\.!~?&= +\*'(),\/\#\:]+)((?!\<\/\w\>))*?");
            MatchCollection collection = reg.Matches(html);

            List<String> list = new List<String>();

            foreach (Match match in collection)
            {
                list.Add(match.Value);
            }

            return list;
        }


    }
}

