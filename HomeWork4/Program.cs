﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork4
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите url сайта");
            String site = Console.ReadLine();
            
            Parser parser = new Parser();
            String html = parser.GetHtml(site);

            Console.WriteLine();
            Console.WriteLine("Найденные url на данном сайте : ");
            List<String> list = parser.ParseUrl(html);
            foreach (String item in list)
            {
                Console.WriteLine(item);
            }

            Console.ReadKey();
        }
    }
}
